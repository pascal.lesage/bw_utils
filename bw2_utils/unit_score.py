from brightway2 import *
import pyprind
import hashlib

def add_unit_score_exchange_and_cf(method):
    """ Add unit score biosphere exchanges and cfs to biosphere3 and methods

    Allows the storing of LCIA results in the B matrix for LCI datasets.
    """
    assert method in methods, "Method {} not in methods".format(method)
    m = Method(method)
    ef_code = m.get_abbreviation()
    ef_name = 'Unit impact for {}'.format(method)
    # Add to biosphere3, skip if already present
    try:
        ef = get_activity(('biosphere3', ef_code))
        assert ef['name'] == ef_name
    except:
        ef = Database('biosphere3').new_activity(code=ef_code)
        ef['name'] = ef_name
        ef['unit'] = m.metadata['unit']
        ef['categories'] = ('undefined',)
        ef['exchanges']: []
        ef['type'] = 'unit exchange'
        ef.save()
        try:
            mapping[('biosphere3', ef_code)]
        except KeyError:
            print("Manually added {} to mapping".format(ef_code))
            mapping.add(('biosphere3', ef_code))
    # Add to associated method, skip if already present
    loaded_method = m.load()
    try:
        existing_cf = [
            cf_tuple for cf_tuple in loaded_method
            if cf_tuple[0] == ('biosphere3', ef_code)
        ][0]
        assert existing_cf[1] == 1
    except:
        loaded_method.append((('biosphere3', m.get_abbreviation()), 1))
        Method(method).write(loaded_method)

def add_all_unit_score_exchanges_and_cfs():
    """Add unit scores and cfs for all methods in project."""
    print("Adding unit score biosphere exchanges and characterization factors "
          "to all {} methods in project".format(len(methods))
          )
    for method in pyprind.prog_bar(methods):
        add_unit_score_exchange_and_cf(method)

def copy_stripped_activity(data, database_name):
    """ Create an activity in `Database(database_name)` with only a production flow.

    Typical usage is the generation of an aggregated dataset: unit impacts would
    then be added to the activity as required, as biosphere exchanges.
    """

    required_fields = [
        'name',
        'reference product',
        'production amount',
        'location',
        'unit'
    ]
    missing_fields = [field for field in required_fields if field not in data]

    assert not missing_fields, "Fields missing: {}".format(missing_fields)

    if data.get('code'):
        new_code = data['code']
    else:
        s = "".join([data[field] for field in required_fields])
        new_code = str(hashlib.md5(s.encode('utf-8')).hexdigest())

    if database_name not in databases:
        Database(database_name).register()

    act = Database(database_name).new_activity(
        code=new_code,
        name=data['name'],
        location=data['location'],
        unit=data['unit'],
    )
    act['reference product'] = data['reference product']
    act['production amount'] = data['production amount']

    act.save()
    exc = act.new_exchange(
        name=data['reference product'],
        location=data['location'],
        input=act.key,
        output=act.key,
        amount=data['production amount'],
        type="production"
    )
    exc.save()
    return act


def copy_empty_up_activity_to_agg_database(up_act_key, agg_database,
                                           make_db=False,
                                           exist_ok=False):
    """Copy a stripped unit process activity to a new database

    The copy will contain a production exchange and metadata, but no
    biosphere or technosphere exchanges.

    Typical useage: first step to creating an aggregated dataset."""

    # Make sure there is actually a unit process activity to copy
    try:
        up_act = get_activity(up_act_key)
    except:
        raise ValueError("{} does not exist as an activity key")

    # Check existence of aggregated database
    if agg_database not in databases:
        if not make_db:
            raise ValueError("{} not in databases, create first or set make_db to True")
        else:
            Database(agg_database).register()
    # Check if aggregated dataset already exists
    try:
        get_activity((agg_database, up_act_key[1]))
        agg_act_exists = True
    except:
        agg_act_exists = False
    if agg_act_exists:
        if not exist_ok:
            raise ValueError("Aggregated dataset already exists")
        else:
            return # Asssumption that if it exists, all is fine.






def add_unit_impact_to_act(activity_code, up_database, agg_database, method, overwrite=False, ):
    """Store LCIA score for activity in biosphere exchange

    If necessary, will create unit score exchanges and characterization factors

    """