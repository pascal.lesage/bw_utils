# -*- coding: utf-8 -*-
"""
Functions to create and setup Brightway2 projects.
Because I'm lazy.

For use with the Brightway2 package: https://brightwaylca.org/

"""

from brightway2 import *

def initProject_ei32_CU_new(project_name,
                      database_path =\
                      "C:\\mypy\\data\\ecoinvent 3_2 CutOff\\datasets",\
                      databaseName = "ei32_CU_U"\
                      ):
    assert type(project_name) == str, "You need to provide a valid project name (str)"
    projects.set_current(project_name)
    bw2setup()
    
    if "ei32_CU_U" not in databases:
        ei = SingleOutputEcospold2Importer(database_path, databaseName)
        ei.apply_strategies()
        ei.write_database()
    print("Current project: {}".format(projects.current))
    print("Databases in project: {}".format(list(databases)))
	
def initProject_ei32_CU_from_within(database_path =\
                      "C:\\mypy\\data\\ecoinvent 3_2 CutOff\\datasets",\
                      databaseName = "ei32_CU_U"\
                      ):
                      
    bw2setup()
    if "ei32_CU_U" not in databases:
        ei = SingleOutputEcospold2Importer(database_path, databaseName)
        ei.apply_strategies()
        ei.write_database()
    print("Current project: {}".format(projects.current))
    print("Databases in project:".format(list(databases)))