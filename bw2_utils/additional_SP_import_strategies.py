# -*- coding: utf-8 -*-
"""
Some ad-hoc strategies to get SimaPro CSV exports to import in Brightway.

Very project specific, may not be of much relevance in other circumstances.

"""
from brightway2 import Database
from collections import defaultdict
import copy

def electricityUnitStrategy(SP_CSVimporter): 
    ''' Changes SimaPro CSV importer in place, replacing electricity exchanges
        that are expressed in MJ to electricity exchanges expressed in kWh'''
    electricity_exchanges_dict = defaultdict(list)
    for dataset in SP_CSVimporter:
        for exchange in dataset['exchanges']:
            if ('Electricity' in exchange['name'] or 'electricity' in exchange['name']) and exchange['unit'] == 'MJ':
                electricity_exchanges_dict[dataset['code']].append(exchange)
    modified_electricity_exchanges_dict = copy.deepcopy(electricity_exchanges_dict)
    for k in modified_electricity_exchanges_dict.keys():
        modified_electricity_exchanges_dict[k][0]['amount'] = modified_electricity_exchanges_dict[k][0]['amount']/3.6
        modified_electricity_exchanges_dict[k][0]['unit'] = 'kWh'
    for dataset in SP_CSVimporter.data:
        if dataset['code'] in electricity_exchanges_dict.keys():
            for values, modValues in zip(electricity_exchanges_dict[dataset['code']], 
                                         modified_electricity_exchanges_dict[dataset['code']]):
                index = dataset['exchanges'].index(values)
                dataset['exchanges'][index] = modValues
                
def waterStrategy(SP_CSVimporter): 
    ''' Changes SimaPro CSV importer in place, replacing 'Water, country' emissions to air in kg
        to simple 'water' emissions in m3. 
        Used in Nestle project'''
    regionalized_water = ['Water, AR','Water, BR','Water,CN',
                      'Water, DE','Water, FR','Water, GB',
                      'Water, IN','Water, NL','Water, NZ',
                      'Water, PK','Water, PL','Water, RU',
                      'Water, TR','Water, US']

    water_exchanges_dict = defaultdict(list)
    for dataset in SP_CSVimporter:
        for exchange in dataset['exchanges']:
            if exchange['name'] in regionalized_water:
                water_exchanges_dict[dataset['code']].append(exchange)
    modified_water_exchanges_dict = copy.deepcopy(water_exchanges_dict)
    for k in modified_water_exchanges_dict.keys():
        if modified_water_exchanges_dict[k][0]['unit'] == 'kilogram':
            modified_water_exchanges_dict[k][0]['amount'] = modified_water_exchanges_dict[k][0]['amount']/1000
            modified_water_exchanges_dict[k][0]['unit'] = 'm3'            
        modified_water_exchanges_dict[k][0]['name'] = 'Water'
    for dataset in SP_CSVimporter.data:
        if dataset['code'] in water_exchanges_dict.keys():
            for values, modValues in zip(water_exchanges_dict[dataset['code']], 
                                         modified_water_exchanges_dict[dataset['code']]):
                index = dataset['exchanges'].index(values)
                dataset['exchanges'][index] = modValues

def delete_annoying_biosphere_flows(SP_CSVimporter):
    '''Use as last resort. Will delete biosphere flows that are not in the biosphere3 database.'''
    # for dataset in SP_CSVimporter:
        # for exchange in dataset['exchanges']:
            # if exchange['type'] == 'biosphere':
                # if 'input' not in exchange or 'amount' not in exchange:
                    # print("deleting {}-{}".format(dataset['name'], exchange))
                    # del exchange
    data = SP_CSVimporter.data
    for index, (key, ds) in enumerate(data.items()):
        for exchange in ds.get('exchanges', []):
            if 'input' not in exchange or 'amount' not in exchange:
                del exchange
            if 'type' not in exchange:
                del exchange
                
                
def unitImpactExchangeConverter(SP_CSVimporter, virtualImpactFlowsDict): 
    ''' Changes SimaPro CSV importer in place, replacing some flows used in a specific project 
        and that were not  reckonized by Brightway into "unit impact" flows.
        Requires an "augmentation of the biosphere3 database using
        the addUnitImpactFlowsToBiosphereAndMethods() function.'''
    
    unitImpactExchangeInSP = defaultdict(list)
    bioLoaded = Database('biosphere3').load()
    for dataset in SP_CSVimporter:
        for exchange in dataset['exchanges']:
            if exchange['name'] in virtualImpactFlowsDict.keys():
                unitImpactExchangeInSP[dataset['code']].append(exchange)
    modified_unitImpactExchangeInSP = copy.deepcopy(unitImpactExchangeInSP)
    for dataset in modified_unitImpactExchangeInSP.keys():
        for exchange in modified_unitImpactExchangeInSP[dataset]:
            exchange['categories'] = ('undefined',)
            exchange['input'] = ('biosphere3', virtualImpactFlowsDict[exchange['name']])
            exchange['name'] = bioLoaded[('biosphere3', virtualImpactFlowsDict[exchange['name']])]['name'] 
    for dataset in SP_CSVimporter.data:
        if dataset['code'] in unitImpactExchangeInSP.keys():
            for values, modValues in zip(unitImpactExchangeInSP[dataset['code']], 
                                         modified_unitImpactExchangeInSP[dataset['code']]):
                index = dataset['exchanges'].index(values)
                dataset['exchanges'][index] = modValues
                
def correctUncertaintyInformation(SP_CSVimporter): 
    ''' Brightway imports lognormal distribution parameter "GSD2" as `shape` rather than `scale`.'''
    
    for dataset in SP_CSVimporter:
        for exchange in dataset['exchanges']:
            try:
                if exchange['uncertainty type'] == (2 or 3):
                    exchange['scale'] = exchange['shape']
                    del exchange['shape']
            except KeyError:
                pass