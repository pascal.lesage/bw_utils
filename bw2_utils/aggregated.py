# -*- coding: utf-8 -*-
"""Helper functions for generating and using aggregated datasets in brightway2

Notably:
    -

"""
import brightway2 as bw
from .checks import database_exists, act_exists, method_exists, \
    single_production_exchange_exists
import pyprind
import hashlib
import copy
import numpy as np

def add_unit_score_exchange_and_cf(method):
    """ Add unit score biosphere exchanges and cfs to biosphere3 and methods

    Allows the storing of LCIA results in the B matrix for LCI datasets.
    """
    assert method_exists(method), "Method {} not in methods".format(method)
    m = bw.Method(method)
    ef_code = m.get_abbreviation()
    ef_name = 'Unit impact for {}'.format(method)
    # Add to biosphere3, skip if already present
    try:
        ef = bw.get_activity(('biosphere3', ef_code))
        assert ef['name'] == ef_name
    except:
        ef = bw.Database('biosphere3').new_activity(code=ef_code)
        ef['name'] = ef_name
        ef['unit'] = m.metadata['unit']
        ef['categories'] = ('undefined',)
        ef['exchanges']: []
        ef['type'] = 'unit exchange'
        ef.save()
        try:
            bw.mapping[('biosphere3', ef_code)]
        except KeyError:
            print("Manually added {} to mapping".format(ef_code))
            bw.mapping.add(('biosphere3', ef_code))
    # Add to associated method, skip if already present
    loaded_method = m.load()
    try:
        existing_cf = [
            cf_tuple for cf_tuple in loaded_method
            if cf_tuple[0] == ('biosphere3', ef_code)
        ][0]
        assert existing_cf[1] == 1
    except:
        loaded_method.append((('biosphere3', m.get_abbreviation()), 1))
        bw.Method(method).write(loaded_method)

def add_all_unit_score_exchanges_and_cfs():
    """Add unit scores and cfs for all methods in project."""
    print("Adding unit score biosphere exchanges and characterization factors "
          "to all {} methods in project".format(len(bw.methods))
          )
    for method in pyprind.prog_bar(bw.methods):
        add_unit_score_exchange_and_cf(method)

def copy_stripped_activity_to_other_db(data, target_db, exist_ok=False,
                                       create_database_on_fly=False):
    """ Create an activity in `Database(target_db)` with only a production flow.

    Typical usage is the generation of an aggregated dataset: unit impacts would
    then be added to the activity as required, as biosphere exchanges.
    """
    # Check that the target database
    if not database_exists(target_db):
        if not create_database_on_fly:
            raise ValueError("Database {} does not exist, set "
                             "`create_database_on_fly` to automatically create "
                             "database")
        else:
            bw.Database(target_db).register()

    # Check that all the required data is present
    required_fields = [
        'name',
        'reference product',
        'production amount',
        'location',
        'unit'
    ]
    missing_fields = [field for field in required_fields if field not in data]
    assert not missing_fields, "Fields missing: {}".format(missing_fields)

    if data.get('code'):
        new_code = data['code']
    else:
        s = "".join([data[field] for field in required_fields])
        new_code = str(hashlib.md5(s.encode('utf-8')).hexdigest())

    # Create or update target activity
    if act_exists((target_db, new_code)):
        if not exist_ok:
            raise ValueError("Activity already exists in target database")
        else:
            act = bw.get_activity((target_db, new_code))
            act['location'] = data['location']
            act['name'] = data['name']
            act['unit'] = data['unit']
    else:
        act = bw.Database(target_db).new_activity(
            code=new_code,
            name=data['name'],
            location=data['location'],
            unit=data['unit'],
        )

    act['reference product'] = data['reference product']
    act['production amount'] = data['production amount']
    act.save()

    # Create or update production exchange
    if single_production_exchange_exists((target_db, new_code)):
        exc = [p for p in act.production()][0]
        exc['name'] = data['reference product'],
        exc['location'] = data['location'],
        exc['input'] = act.key,
        exc['output'] = act.key,
        exc['amount'] = data['production amount'],
        exc['type'] = "production"
    else:
        exc = act.new_exchange(
            name=data['reference product'],
            location=data['location'],
            input=act.key,
            output=act.key,
            amount=data['production amount'],
            type="production"
        )
    exc.save()
    return act

def add_impact_scores_to_act(act_code, agg_db, up_db, selected_methods,
                             overwrite=False, create_ef_on_the_fly=False):
    """ Add unit impact scores to biosphere exchanges of activity in agg database

    The up_db is the unit process level database used for the calculations.
    The elementary flow code is Method(method).get_abbreviation()
    The elementary flow unit is Method(method).metadata['unit']
    The elementary flow name is 'Unit impact for {}'.format(method)
    """
    # Make sure unit process dataset exists
    assert act_exists((up_db, act_code)), "Unit process dataset missing"
    up_act = bw.get_activity((up_db, act_code))
    # Create aggregated dataset if required
    if not act_exists((agg_db, act_code)):
        agg_act = copy_stripped_activity_to_other_db(copy.deepcopy(up_act._data), agg_db)
    else:
        agg_act = bw.get_activity((agg_db, act_code))

    existing_biosphere_in_agg = [exc.input.key for exc in agg_act.biosphere()]
    up_production_amount = up_act['production amount']

    lca = bw.LCA({up_act:up_production_amount})
    lca.lci()
    for method in selected_methods:
        m = bw.Method(method)
        ef_code = m.get_abbreviation()
        ef_name = 'Unit impact for {}'.format(method)
        result_already_in_act = ('biosphere3', ef_code) in existing_biosphere_in_agg
        if result_already_in_act:
            print("Results already exist for activity {}, category {}".format(agg_act, selected_methods))
            if not overwrite:
                print("Set overwrite=True to replace value")
                return None
            if overwrite:
                potential_exc = [exc for exc in agg_act.biosphere() if exc.input.key == ('biosphere3', ef_code)]
                if len(potential_exc) > 1:
                    raise ValueError(
                        "More than one corresponding exchange found. activity: {}, exchange:{}".format(
                            agg_act, method
                        )
                    )
                else:
                    exc = potential_exc[0]
        if not act_exists(('biosphere3', ef_code)):
            if not create_ef_on_the_fly:
                raise ValueError('{} needs to be added to biosphere database'.format(ef_name))
            else:
                add_unit_score_exchange_and_cf(method)

        lca.switch_method(method)
        lca.lcia()
        if not result_already_in_act:
            exc = agg_act.new_exchange(
                input=('biosphere3', ef_code),
                output=agg_act.key,
                amount=lca.score,
                unit=m.metadata['unit'],
            )
            exc['type'] = 'biosphere'
        else:
            exc['amount'] = lca.score
        exc.save()

def calculate_LCIA_array_from_LCI_array(LCI_array, method, ref_bio_dict, result_precision='float32'):
    """ Calculate a 1xn array of LCIA results from existing mxn LCI results array

    The reference biosphere dictionary (ref_bio_dict) provides a mapping between
    biosphere exchange keys and their corresponding rows in the LCI
    array, i.e. its values are (bio_db_name, code): row_number_in_LCI_array
    """
    # Get a list of elementary flows that are characterized in the given method
    loaded_method = bw.Method(method).load()
    method_ordered_exchanges = [exc[0] for exc in loaded_method]

    # Collectors for the LCI array indices and characterization factors that
    # are relevant for the impact assessment (i.e. those that have
    # characterization factors for the given method)
    lca_specific_biosphere_indices = []
    cfs = []
    for exc in method_ordered_exchanges:  # For every exchange that has a cf
        try:
            # Check to see if it is in the bio_dict
            # If it is, it is in the inventory, and its index is bio_dict[exc]
            lca_specific_biosphere_indices.append(ref_bio_dict[exc])
            # If it is in bio_dict, we need its characterization factor
            cfs.append(dict(loaded_method)[exc])
        except KeyError: # Exchange was not in bio_dict
            pass

    # Extract elements of the LCI array that are characterized,
    # in the correct order
    filtered_LCI_array = LCI_array[lca_specific_biosphere_indices][:]
    # Convert CF list to CF array
    cf_array = np.reshape(np.array(cfs), (-1, 1))
    # LCIA score = sum of multiplication of inventory result and CF
    LCIA_array = (np.array(filtered_LCI_array) * cf_array).sum(axis=0)
    # Change result precision if needed
    if LCIA_array.dtype != result_precision:
        LCIA_array = LCIA_array.astype(result_precision, copy=False)
    return LCIA_array


class IterativeAggregatedDatabaseGenerator(object):
    def __init__(self,
                 from_db_name,
                 to_db_name,
                 database_type='LCI',
                 method_list=list(bw.methods),
                 overwrite=False
                 ):
        """ Generator to produce data required to produce aggregated database

        Use:
            new_data = IterativeAggregatedDatabaseGenerator(
                from_db_name,
                to_db_name,
                database_type,
                methods_list,
                overwrite)
            bw.Database(to_db_name).write(new_data)

        Args:
            from_db_name: name of the unit process database from which results will be calculated
            to_db_name: name of the new aggregated database
            database_type:
                "LCI": cradle-to-gate inventories saved.
                    Note that performance of LCI aggregated databases is very poor
                    due to Brightway2 assuming sparse matrices
                "LCIA": scores saved as "unit impact" biosphere exchanges
            method_list: list of method ids (tuples) for which to generate LCIA scores
            overwrite: if False and aggregated database already exists, ValueError is raised
        """
        assert database_exists(from_db_name), "Source database does not exist"
        if database_exists(to_db_name) and not overwrite:
            print("A database named {} already exists, set `overwrite` to True to overwrite")
            return
        self.source = bw.Database(from_db_name)
        self.new_name = to_db_name
        self.lca = bw.LCA({self.source.random(): 1})
        self.lca.lci(factorize=True)
        self.database_type = database_type
        self.methods = method_list
        if self.database_type not in ['LCI', 'LCIA']:
            raise ValueError(
                '{} is not a valid database type, should be "LCI" or "LCIA"'.format(
                    self.database_type
                )
            )
        if self.database_type == "LCIA":
            assert self.methods
            for m in self.methods:
                add_unit_score_exchange_and_cf(m)
            self.C_matrices = {}

    def __len__(self):
        return len(self.source)

    def __iter__(self):
        # Data for this line:
        # wrong_database = {key[0] for key in data}.difference({self.name})
        yield ((self.new_name,))

    def get_exchanges(self):
        vector = self.lca.inventory.sum(axis=1)
        assert vector.shape == (len(self.lca.biosphere_dict), 1)
        return [{
            'input': flow,
            'amount': float(vector[index]),
            'type': 'biosphere',
        } for flow, index in self.lca.biosphere_dict.items()
            if abs(float(vector[index])) > 1e-17]

    def create_C_matrices(self):
        self.C_matrices = {}
        for method in self.methods:
            self.lca.switch_method(method)
            self.C_matrices[method] = self.lca.characterization_matrix
        return self.C_matrices

    def get_impacts(self):
        if len(self.C_matrices) == 0:
            self.create_C_matrices()
        return [{
            'input': ('biosphere3', bw.Method(method).get_abbreviation()),
            'amount': (C_matrix * self.lca.inventory).sum(),
            'type': 'biosphere',
            'name': 'Unit impact for {}'.format(method),
            'unit': bw.Method(method).metadata['unit']
        } \
            for method, C_matrix in self.C_matrices.items()]

    def keys(self):
        # Data for this line:
        # mapping.add(data.keys())
        for act in self.source:
            yield (self.new_name, act['code'])

    def values(self):
        # Data for this line:
        # geomapping.add({x["location"] for x in data.values() if x.get("location")})
        for act in self.source:
            yield act

    def items(self):
        # Actual data which is consumed by the function writing to the database
        for act in self.source:
            self.lca.redo_lci({act: act['production amount']})
            obj = copy.deepcopy(act._data)
            obj['database'] = self.new_name
            if self.database_type == "LCI":
                obj['exchanges'] = self.get_exchanges()
            elif self.database_type == "LCIA":
                obj['exchanges'] = self.get_impacts()
                assert len(obj['exchanges']) == len(self.methods)
            else:
                raise ValueError('Database type not understood')
            obj['exchanges'].append(
               {
                   'input': (self.new_name, act['code']),
                   'type': 'production',
                   'amount': act['production amount'],
               }
            )
            yield ((self.new_name, obj['code']), obj)

    def generate(self):
        bw.Database(self.new_name).write(self)
