def plot_comparison(dict_for_plotting, method, product1, product2):
    
    data = dict_for_plotting[list(dict_for_plotting.keys())[0]]
    
    fig,ax = plt.subplots(1)
    for case in data.keys():
        if case != 'deterministic':
            x=data[case][method]
            comparison=(x[np.where( x < 0 )].size)/(x.size)*100
            if comparison > 50:
                label="{} :{}{}{} {}% of time".format(case,str(product1), "<", str(product2),comparison)
            else:
                label="{} :{}{}{} {}% of time".format(case, str(product2), "<", str(product1),comparison)
            #sns.distplot(x, hist=False, rug=False, norm_hist=True, label = label)
            sns.distplot(x, hist=False, rug=True, norm_hist=False, label = label).set_xlim(np.percentile(x, 0.01), np.percentile(x, 99.99))

        else:
            if data[case][method]<0:
                det_label = "{}: {} < {}".format(case, product1, product2)
            elif data[case][method]>0:
                det_label = "{} < {}".format(product2, product1)
            else:
                det_label = "{} and {} are equivalent".format(product1, product2)
            ax.plot(data[case][method], 0, "bs", label = det_label)
            #ax.plot([data[case][method], data[case][method]], [0, 0.01], label = det_label)
            
            
    handles,labels = ax.get_legend_handles_labels()
    det_hand=handles[labels.index(det_label)]
    labels.remove(det_label)
    labels.insert(0, det_label)
    handles.remove(det_hand)
    handles.insert(0, det_hand)
    plt.legend(handles,labels,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel((Method(method).name, Method(method).metadata["unit"]), fontsize=10)
    plt.title('Comparative LCA {}'.format(list(dict_for_plotting.keys())[0]))
    return fig