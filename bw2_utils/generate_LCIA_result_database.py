# -*- coding: utf-8 -*-
"""
Functions that create "LCI" or "LCIA" version of a database for /fast/ calculations in future.
For use with the Brightway2 package: https://brightwaylca.org/

Approach:
    Calculate LCI or LCIA for one unit of each product in a given database
    Store results in new database:
        LCI: store cradle-to-gate inventories.
        LCIA: store "unit impact" flows for each impact category.
        The latter assumes that the biosphere3 database and methods have been 
        augmented with unit impact exchanges and characterization factors, 
        respectively.
    
Much of the code was initially developed to create an LCI version of
ecoinvent3, initiated by Pascal Lesage and much improved by 
Chris Mutel.

Note that the use of aggregated LCI datasets is not efficient in Brightway, due to the 
way matrices are built.

"""
from brightway2 import *
import numpy as np
import copy
import itertools

class IterativeSystemGenerator(object):
    def __init__(self, 
                 from_db_name, 
                 to_db_name, 
                 database_type = 'LCI', 
                 listMethods = list(methods)
                 ):
        self.source = Database(from_db_name)
        self.new_name = to_db_name
        self.lca = LCA({self.source.random(): 1})
        self.lca.lci(factorize=True)
        self.database_type = database_type
        self.methods = listMethods
        assert self.database_type in ['LCI', 'LCIA'],\
            '{} is not a valid database type, should be "LCI" or "LCIA"'.format(
            self.database_type
            )
        if self.database_type == "LCIA":
            assert Database('biosphere3').search('Unit impact'),\
            "Can't create LCIA database: the biosphere3 \
            database has not yet been augmented"
            self.C_matrices = {}
    def __len__(self):
        return len(self.source)

    def __iter__(self):
        # Data for this line:
        # wrong_database = {key[0] for key in data}.difference({self.name})        
        yield ((self.new_name,))
    
    def get_exchanges(self):
        vector = self.lca.inventory.sum(axis=1)
        assert vector.shape == (len(self.lca.biosphere_dict), 1)
        return [{
                    'input': flow,
                    'amount': float(vector[index]),
                    'type': 'biosphere',
                } for flow, index in self.lca.biosphere_dict.items()
                if abs(float(vector[index])) > 1e-17]

    def create_C_matrices(self):
        self.C_matrices = {}
        for method in self.methods:
            self.lca.switch_method(method)
            self.C_matrices[method] = self.lca.characterization_matrix
        return self.C_matrices

    def get_impacts(self):
        if len(self.C_matrices) == 0:
            self.create_C_matrices()
        return [{
                    'input': ('biosphere3', Method(method).get_abbreviation()),
                    'amount': (C_matrix * self.lca.inventory).sum(),
                    'type': 'biosphere'                                
                                }\
                                for method, C_matrix in self.C_matrices.items()]
                                
        
    def keys(self):
        # Data for this line:
        # mapping.add(data.keys())
        for act in self.source:
            yield (self.new_name, act['code'])

    def values(self):
        # Data for this line:
        # geomapping.add({x["location"] for x in data.values() if x.get("location")})  
        for act in self.source:
            yield act
            
    def items(self):
        # Actual data which is consumed by the function writing to the database
        for act in self.source:
            self.lca.redo_lci({act: 1})
            obj = copy.deepcopy(act._data)
            obj['database'] = self.new_name
            if self.database_type == "LCI":
                obj['exchanges'] = self.get_exchanges()
                yield ((self.new_name, obj['code']), obj)
            elif self.database_type == "LCIA":
                obj['exchanges'] = self.get_impacts()
                assert len(obj['exchanges'])==len(self.methods)
                yield ((self.new_name, obj['code']), obj)

def generate_aggregated_database(from_db_name,
                                  to_db_name,
                                  database_type='LCI',
                                  listMethods=list(methods)):
    #print("start")
    #print(listMethods)
    new_data = IterativeSystemGenerator(from_db_name,
                                        to_db_name,
                                        database_type,
                                        listMethods)
    #print("new data successfully initiated.")    
    Database(to_db_name).write(new_data)

#generate_LCIA_result_database('ei32_CU_U', 'ei32_CU_LCIA', database_type='LCIA', listMethods=[('ReCiPe Endpoint (E,A) w/o LT', 'ecosystem quality w/o LT', 'terrestrial acidification w/o LT')])
#ei32_LCIA = Database('ei32_LCIA')
#ld = ei32_LCIA.load()