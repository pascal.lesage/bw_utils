import uuid
from brightway2 import *
from bw2_utils import pedigree


def introduce_application_uncertainty_filter(exchange, new_pedigree_dict):
    """ Create a filter activities that interposes two existing activities to add application uncertainty.
        Modify the upstream uncertainty so its input now points to the filter.
        """
    
    # Create filter activity
    new_code = str(uuid.uuid4())
    filter_act = Database(exchange.output['database']).new_activity(new_code)
    filter_act['name'] = "filter activity between {} and {}".format(exchange.input, exchange.output)
    filter_act['comment'] = "Filter dataset that interposes itself between consuming activity {} and a producing activity {}, with the sole purpose of adding application uncertainty".format(exchange.input, exchange.output)
    filter_act.save()
    
    # Add input exchange with uncertainty
    input_exc = filter_act.new_exchange()
    input_exc['input'] = exchange.input
    input_exc['name'] = exchange['name']
    input_exc['amount'] = 1
    input_exc['unit'] = exchange['unit']
    input_exc['pedigree'] = new_pedigree_dict 
    input_exc['uncertainty type'] = 2
    try: 
        input_exc['scale without pedigree'] = exchange['scale without pedigree']
    except KeyError:
        input_exc['scale without pedigree'] = 0
    
    input_exc['scale'] = pedigree.calculate_uncertainty(input_exc['scale without pedigree'], input_exc['pedigree'])
    input_exc['type'] = 'technosphere'
    input_exc['loc'] = 0
    input_exc.save()

    # Add output exchange
    output_exc = filter_act.new_exchange()
    output_exc['input'] = filter_act.key
    output_exc['output'] = filter_act.key
    output_exc['amount'] = 1
    output_exc['type'] = 'production'
    output_exc.save()

    #Change input in consuming act
    exchange['input'] = filter_act.key
    exchange.save()

    return filter_act