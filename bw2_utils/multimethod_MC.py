"""Function to quickly set-up a Monte Carlo calculation with multiple methods"""

from brightway2 import *
from .get_C_matrices import get_C_matrices
import numpy as np

def multimethod_MC(demand, method_list, iterations):
    MC_results_dict = {}
    C_matrices = get_C_matrices(list(demand.keys())[0], method_list)
    for method in method_list:
        MC_results_dict[method] = np.zeros((1, iterations))
    
    MC = MonteCarloLCA(demand)
    for iteration in range(iterations):
        next(MC)
        for method in method_list:
            MC_results_dict[method][0, iteration] = (C_matrices[method]*MC.inventory).sum()
    return MC_results_dict