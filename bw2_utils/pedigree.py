"""
Functions surrounding the use of the Pedigree matrix in Brightway2.
* Calculate the additional and total uncertainty from pedigree scores.
* Modify pedigree scores.
* Add "filter" activities adding uncertainty to an entire downstream activity (application uncertainty).
* Prioritize data collection for a given exchange.
* Prioritize uncertainty for a given activity.
"""
from brightway2 import *
import operator
import numpy as np

""" Note: data_quality_uncertainty_factors are expressed as variances of the underlying normal distributions.
    This differs from the exchange['scale without pedigree'], which is expressed as the standard deviation
    of the underlying uncertainty in Brightway2 import of ecoinvent."""
data_quality_uncertainty_factors = {
        'ecoinvent_32_pedigree_matrix': 
            {
                'reliability': 
                    {
                    1:0.0,
                    2:0.0006,
                    3:0.002,
                    4:0.008,
                    5:0.04
                    },
                'completeness':
                    {
                    1: 0.0,
                    2: 0.0001,
                    3: 0.0006,
                    4: 0.002,
                    5: 0.008
                    },
                'temporal correlation':
                    {
                    1:0.0,
                    2:0.0002,
                    3:0.002,
                    4:0.008,
                    5:0.04
                    },
                'geographical correlation':
                    {
                    1:0.0,
                    2:0.000025,
                    3:0.0001,
                    4:0.0006,
                    5:0.002
                    },
                'further technological correlation':
                    {
                    1:0.0,
                    2:0.0006,
                    3:0.008,
                    4:0.04,
                    5:0.12
                    }
                },
        'Muller et al': ## Values not yet entered!!
            {
                'reliability': 
                    {
                    1:0.0,
                    2:0.0006,
                    3:0.002,
                    4:0.008,
                    5:0.04
                    },
                'completeness':
                    {
                    1: 0.0,
                    2: 0.0001,
                    3: 0.0006,
                    4: 0.002,
                    5: 0.008
                    },
                'temporal correlation':
                    {
                    1:0.0,
                    2:0.0002,
                    3:0.002,
                    4:0.008,
                    5:0.04
                    },
                'geographical correlation':
                    {
                    1:0.0,
                    2:0.000025,
                    3:0.0001,
                    4:0.0006,
                    5:0.002
                    },
                'further technological correlation':
                    {
                    1:0.0,
                    2:0.0006,
                    3:0.008,
                    4:0.04,
                    5:0.12
                    }
                }
            }
            
def create_pedigree_dict(reliability= 1,
                        completeness= 1,
                        temporal_correlation= 1,
                        geographical_correlation= 1,
                        further_technological_correlation= 1):
    """Default scores are 1""" 
    assert all(isinstance(score, int) for score in [reliability, completeness, temporal_correlation, geographical_correlation,further_technological_correlation]), "Scores need to be integers"
    assert all(1<=score<=5 for score in [reliability, completeness, temporal_correlation, geographical_correlation,further_technological_correlation]), "Scores need to be between 1 and 5"
    return {'reliability': reliability, 
            'completeness': completeness, 
            'temporal correlation': temporal_correlation, 
            'geographical correlation': geographical_correlation,
            'further technological correlation': further_technological_correlation
            }

def modify_pedigree_dict(existing_pedigree_dict, 
                        reliability= None,
                        completeness= None,
                        temporal_correlation= None,
                        geographical_correlation= None,
                        further_technological_correlation= None):
    new_scores = [score for score in [reliability, completeness, temporal_correlation, geographical_correlation,further_technological_correlation] if score is not None] 
    assert all(isinstance(score, int) for score in new_scores), "New scores need to be integers"
    assert all(1<=score<=5 for score in new_scores), "Scores need to be between 1 and 5"
    modified_pedigree_dict = existing_pedigree_dict
    if reliability:
        modified_pedigree_dict['reliability']= reliability
    if completeness:
        modified_pedigree_dict['completeness']= completeness
    if temporal_correlation:
        modified_pedigree_dict['temporal correlation']= temporal_correlation
    if geographical_correlation:
        modified_pedigree_dict['geographical correlation']= geographical_correlation
    if further_technological_correlation:
        modified_pedigree_dict['further technological correlation']= further_technological_correlation        
    return modified_pedigree_dict
            
def calculate_uncertainty(basic_uncertainty, pedigree_scores_as_dict, source_uncertainty_factors= 'ecoinvent_32_pedigree_matrix'):
    """ The basic uncertainty is expressed as the standard deviation of the underlying normal distribution."""
    
    uncertainty_factors = data_quality_uncertainty_factors[source_uncertainty_factors]
    sum_of_additional_uncertainty_variances = sum(
                                            [uncertainty_factors[characteristic][score] 
                                            for characteristic, score in pedigree_scores_as_dict.items()])
    return np.sqrt(sum_of_additional_uncertainty_variances + basic_uncertainty**2)


def modify_lognormal_uncertainty_information(exchange,
                                             new_pedigree_scores_as_dict= None,
                                             new_basic_uncertainty_as_sd_of_underlying_normal = None,
                                             source_uncertainty_factors= 'ecoinvent_32_pedigree_matrix'):
    """ Changes the exchange in place and does not return anything. Only works for lognormally distributed exchanges.
        Application to other distributions possible but not implemented, see DOI 10.1007/s11367-014-0759-5 """
    #exchange = import_exchange(exchange_input, exchange_output)
    
    assert exchange['uncertainty type'] == 2, ("This function only works for lognormal distributions. The uncertainty information for {} has not been updated".format(exchange))

    if new_pedigree_scores_as_dict == None:
        try:
            exchange['pedigree']
        except KeyError:
            print("The exchange does not have pedigree scores. You must supply one to use this function on this exchange.")
    else:
        if new_pedigree_scores_as_dict:
            exchange['pedigree'] = new_pedigree_scores_as_dict
        if new_basic_uncertainty_as_sd_of_underlying_normal:
            exchange['scale without pedigree'] = new_basic_uncertainty_as_sd_of_underlying_normal
        exchange['scale'] = calculate_uncertainty(exchange['scale without pedigree'], exchange['pedigree'])
        exchange['source of uncertainty factors'] = source_uncertainty_factors
    return exchange

def sort_contributions_to_exchange_uncertainty(exchange, include_basic_uncertainty= False):
    """ Returns the variance of underlying normal for each of the characteristics of interest.
        Scale with additional uncertainty is the square root of the sume of these variances.
        Only works for lognormally distributed exchanges. Application to other distributions possible,
        but not implemented, see DOI 10.1007/s11367-014-0759-5."""
    
    #exchange = import_exchange(exchange_input, exchange_output)
    
    assert exchange['uncertainty type'] == 2, ("This function only works for lognormal distributions. The uncertainty information for {} has not been updated".format(exchange))
    assert 'pedigree' in exchange, "This exchange does not have pedigree scores. Consider adding one with `modify_lognormal_uncertainty_information`"
    
    try:
        source_uncertainty_factors = exchange['source of uncertainty factors']
    except KeyError: 
        source_uncertainty_factors = 'ecoinvent_32_pedigree_matrix'
        print("The ecoinvent 3.2 uncertainty factors are used as default")
       
    uncertainty_factors = {}
    for characteristic, score in exchange['pedigree'].items():
        uncertainty_factors['{}-{}'.format(characteristic, score)]= data_quality_uncertainty_factors[source_uncertainty_factors][characteristic][score]
    
    if include_basic_uncertainty:
        uncertainty_factors.update({"basic uncertainty": exchange['scale without pedigree']**2})
    return sorted(uncertainty_factors.items(), key=operator.itemgetter(1), reverse=True)


#def import_exchange(input_exc, output_exc):
#    act = get_activity(output_exc)
#    exchange = [exc for exc in act.exchanges() if exc['input'] == input_exc][0]
#    return exchange
    