# ARGH!!!
# Does not uniquely identify exchange.
# This is used EVERYWHERE

def import_exchange(input_exc, output_exc):
    act = get_activity(output_exc)
    exchange = [exc for exc in act.exchanges() if exc['input'] == input_exc][0]
    return exchange