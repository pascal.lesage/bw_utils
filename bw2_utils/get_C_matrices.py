"""
Function used to create a dictionary of characterization matrices, allowing
LCA with multiple impact categories to be done mode efficiently (not loading
the methods). Really only useful for Monte Carlo analysis.
Mostly taken from Chris Mutel, personal communication.
"""

from brightway2 import *

def get_C_matrices(demand, list_of_methods):
    C_matrices = {}
    sacrificial_LCA = LCA({demand:1})
    sacrificial_LCA.lci()
    for method in list_of_methods:
        sacrificial_LCA.switch_method(method)
        C_matrices[method] = sacrificial_LCA.characterization_matrix
    return C_matrices