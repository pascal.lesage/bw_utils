from collections import defaultdict

def create_default_dict_for_nesting(list1, list2):
    d = defaultdict(dict)
    for item1 in list1:
        for item2 in list2:
            d[item1][item2] = None
    return d