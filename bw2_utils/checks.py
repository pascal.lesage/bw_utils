# -*- coding: utf-8 -*-
"""Helper functions to run common checks"""
import brightway2 as bw

def act_exists(act_key):
    """Return True if activity exists else False"""
    try:
        bw.get_activity(act_key)
        return True
    except:
        return False

def database_exists(database_name):
    """Return True if database exists else False"""
    return True if database_name in bw.databases else False

def method_exists(m_tuple):
    """Return True if database exists else False"""
    return True if m_tuple in bw.methods else False

def single_production_exchange_exists(act_key):
    """Return True if production exchange exists else False"""
    assert act_exists(act_key)
    act = bw.get_activity(act_key)
    p = [exc for exc in act.production()]
    if len(p)>1:
        raise ValueError("Activity {} has {} production exchanges".format(act_key, len(p)))
    else:
        return True if p else False