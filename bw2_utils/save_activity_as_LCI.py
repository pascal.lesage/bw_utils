from brightway2 import *
import sys
import copy

""" Save unit process as aggregated LCI. Must specify the target database.
    If the target database is the same as the source database, the
    disaggregated activity dataset is overwritten unless user terminates 
    function when prompted."""

def save_activity_as_LCI(disaggregated_activity, target_db):
    
    if disaggregated_activity.key[0] == target_db:
        confirm_overwrite = input("The source and target databases are the same. This will overwrite the disaggrated dataset with its aggregated version. Continue? (y/n)")
        if not confirm_overwrite == "y":
            print("Aborted")
            sys.exit()
    
    agg_ds = copy.deepcopy(disaggregated_activity._data)
    agg_ds['database'] = (target_db)
    
    lca = LCA({disaggregated_activity:1})
    lca.lci()
    vector = lca.inventory.sum(axis=1)
    agg_ds['exchanges'] = [{
                    'input': flow,
                    'amount': float(vector[index]),
                    'type': 'biosphere',
                        } for flow, index in lca.biosphere_dict.items()
                        if abs(float(vector[index])) > 1e-17]
    db = Database(target_db).load()
    db[(agg_ds['database'], agg_ds['code'])] = agg_ds
    Database(target_db).write(db)