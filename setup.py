import setuptools

setuptools.setup(
     name='bw2_utils',
     version='0.1',
     author="Pascal Lesage",
     author_email="pascal.lesage@polymtl.ca",
     description="Utility functions to use with brightway2 package",
     url="https://github.com/PascalLesage/bw_utils",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )